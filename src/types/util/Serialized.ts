import { Either, Maybe } from 'monet'

export type Serialized<T> =
  T extends Maybe<infer U> ? U | null :
  T extends Either<unknown, infer U> ? U :
  T extends Array<infer U> ? Array<Serialized<U>> :
  T extends object ? { [P in keyof T]: Serialized<T[P]> } :
  T
