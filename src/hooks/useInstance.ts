import { useContext } from 'react'
import { InjectionToken } from 'tsyringe'
import { ContainerContext } from '../context/ContainerContext'

export const useInstance = <T>(token: InjectionToken<T>) => {
  const container = useContext(ContainerContext)
  return container.resolve(token)
}
