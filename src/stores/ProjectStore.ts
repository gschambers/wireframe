import { Item } from '../types/domain'

export interface ProjectStore {
  set(items: Item[]): Promise<void>
  get(): Promise<Item[]>
}
